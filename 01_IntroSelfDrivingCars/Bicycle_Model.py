
# This is the Implementation of the 2D-Bicycle Model
import numpy as np

class BicycleModel():
    def __init__(self,L = 2, lr = 1.2, w_max= 1.22,sample_time = 0.01):

        # State Variables
        self.x = 0
        self.y = 0
        self.theta = 0
        self.delta = 0
        self.beta = 0
        # Constants
        self.L = L
        self.lr = lr
        self.w_max = w_max
        self.sample_time = 0.01

    def reset(self):
        # Method for resetting the State Variables
        self.x = 0
        self.y = 0
        self.theta = 0
        self.delta = 0
        self.beta = 0

    def step(self, v, w):
        self.delta = self.delta + w * self.sample_time
        self.beta = np.arctan(self.lr * np.tan(self.delta) / self.L)

        self.theta += (v * np.cos(self.beta) * np.tan(self.delta) / self.L) * self.sample_time
        self.x += v * np.cos(self.theta + self.beta) * self.sample_time
        self.y += v * np.sin(self.theta + self.beta) * self.sample_time
        pass
