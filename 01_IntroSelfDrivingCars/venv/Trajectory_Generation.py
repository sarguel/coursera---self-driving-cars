
# This Script creates a Trajectory with costant velocity to drive around a circle

#1 Constant Velocity and Constant Steering
from Bicycle_Model import BicycleModel
import numpy as np
import matplotlib.pyplot as plt

kstm = BicycleModel()
kstm.delta = np.arctan(2/10)

t_data = np.arange(0,20,0.01)
x_data = np.zeros(2000)
y_data = np.zeros(2000)


for i in range(0,len(t_data)):
    x_data[i] = kstm.x
    y_data[i] = kstm.y
    kstm.step(np.pi,0)

plt.axis('equal')
plt.plot(x_data,y_data)
plt.show()

#2 Costant Velocity

kstm.reset()

t_data = np.arange(0,20,0.01)
x_data = np.zeros(2000)
y_data = np.zeros(2000)

for i in range(0,len(t_data)):
    x_data[i] = kstm.x
    y_data[i] = kstm.y

    kstm.step(np.pi,kstm.delta)

    if (kstm.delta < np.arctan(2/10)):
        kstm.step(np.pi,kstm.w_max)
    else:
        kstm.step(np.pi,0)

plt.axis('equal')
plt.plot(x_data,y_data)
